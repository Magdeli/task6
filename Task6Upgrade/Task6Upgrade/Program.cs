﻿using System;

namespace Task6Upgrade
{
    class Program
    {
        static void Main(string[] args)
        {
                // This program takes the users height and weight and calculates their BMI.

                #region Variables
                // In this region the variables used are declared.

                double height;
                double weight;
                double BMI;

                #endregion

                #region Input
                // In this region the input is obtained, as in the height and weight.

                Console.WriteLine("This is a BMI calculator. Please enter your height in cm:");
                height = double.Parse(Console.ReadLine());

                Console.WriteLine("Please enter your weight in kg:");
                weight = double.Parse(Console.ReadLine());

            #endregion

            #region Calculating the BMI
            // In this region the calculation is done, using the formula BMI = weight(kg)/height(m)^2.
            // The height is gathered from the input as centimeters,
            // so it is divided by 100 to convert it to meters. It uses the method Calculate.

            BMI = Calculate(weight, height);

            #endregion

            #region Returning result
            // In this region the result is returned with the correct description. It uses the method Result.

            Result(BMI);


            // All the possible outcomes are then listed beneath the results. It uses the method Possible.

            Possible();

                #endregion
        }

        public static double Calculate(double weightInput, double heightInput)
        {

            double BMI = weightInput / (Math.Pow((heightInput / 100), 2));

            return BMI;

        }

        public static void Result(double BMI)
        {
            string description;

            if (BMI < 18.5)
            {
                description = "underweight";

                Console.WriteLine($"Your BMI is  {BMI} and this means that you are {description}.");
            }

            if (BMI >= 18.5 && BMI <= 24.9)
            {
                description = "normal";

                Console.WriteLine($"Your BMI is {BMI} and this means that you are {description}.");
            }

            if (BMI >= 25 && BMI <= 29.9)
            {
                description = "overweight";

                Console.WriteLine($"Your BMI is {BMI} and this means that you are {description}.");
            }

            if (BMI >= 30)
            {
                description = "obese";

                Console.WriteLine($"Your BMI is {BMI} and this means that you are {description}.");
            }
        }

        public static void Possible()
        {
            Console.WriteLine();
            Console.WriteLine("The categories are:");
            Console.WriteLine("Underweight - BMI is less than 18.5");
            Console.WriteLine("Normal weight - BMI is 18.5 to 24.9");
            Console.WriteLine("Overweight - BMI is 25 to 29.9");
            Console.WriteLine("Obese - BMI is 30 or more");
        }

    }
 
}
