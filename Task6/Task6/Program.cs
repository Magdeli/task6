﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            // This program takes the users height and weight and calculates their BMI.

            #region Variables
            // In this region the variables used are declared.

            double height;
            double weight;
            double BMI;
            string description;

            #endregion

            #region Input
            // In this region the input is obtained, as in the height and weight.

            Console.WriteLine("This is a BMI calculator. Please enter your height in cm:");
            height = double.Parse(Console.ReadLine());

            Console.WriteLine("Please enter your weight in kg:");
            weight = double.Parse(Console.ReadLine());

            #endregion

            #region Calculating the BMI
            // In this region the calculation is done, using the formula BMI = weight(kg)/height(m)^2.
            // The height is gathered from the input as centimeters,
            // so it is divided by 100 to convert it to meters.

            BMI = weight / (Math.Pow((height / 100), 2));

            #endregion

            #region Returning result
            // In this region the result is returned with the correct description.

            if (BMI < 18.5)
            {
                description = "underweight.";

                Console.WriteLine("Your BMI is " + BMI + " and this means that you are " + description);
            }

            if (BMI >= 18.5 && BMI <= 24.9)
            {
                description = "normal.";

                Console.WriteLine("Your BMI is " + BMI + " and this means that you are " + description);
            }

            if (BMI >= 25 && BMI <= 29.9)
            {
                description = "overweight.";

                Console.WriteLine("Your BMI is " + BMI + " and this means that you are " + description);
            }

            if (BMI >= 30)
            {
                description = "obese.";

                Console.WriteLine("Your BMI is " + BMI + " and this means that you are " + description);
            }

            // All the possible outcomes are then listed beneath the results.

            Console.WriteLine();
            Console.WriteLine("The categories are:");
            Console.WriteLine("Underweight - BMI is less than 18.5");
            Console.WriteLine("Normal weight - BMI is 18.5 to 24.9");
            Console.WriteLine("Overweight - BMI is 25 to 29.9");
            Console.WriteLine("Obese - BMI is 30 or more");

            #endregion
        }
    }
}
